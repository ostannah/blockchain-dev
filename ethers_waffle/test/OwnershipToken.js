const { expect } = require("chai");

describe("OwnershipToken Contract", function() {
  it("Ownership Token Transfer", async function() {

    const [creator, investor] = await ethers.getSigners();
    const TOTAL_SUPPLY = 10;
    const TRANSFER_AMOUNT = 5;

    const OwnershipToken = await ethers.getContractFactory("OwnershipToken");
    const ownershipToken = await OwnershipToken.deploy(TOTAL_SUPPLY); 
    
    await ownershipToken.deployed();

    expect(await ownershipToken.balanceOf(creator.address)).to.equal(TOTAL_SUPPLY);

    await ownershipToken.mint(investor.address, TRANSFER_AMOUNT);

    expect(await ownershipToken.balanceOf(investor.address)).to.equal(TRANSFER_AMOUNT);

  });
});
