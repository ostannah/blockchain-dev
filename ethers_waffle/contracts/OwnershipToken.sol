pragma solidity ^0.5.0;

import "@openzeppelin/contracts/token/ERC20/ERC20Detailed.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20Mintable.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20Burnable.sol";

contract OwnershipToken is ERC20Mintable, ERC20Detailed, ERC20Burnable {
    constructor(uint256 initialSupply) public ERC20Detailed('MadheidiShare', 'MAD', 18) {
        //TODO: is it needed for 0 supply?
        _mint(msg.sender, initialSupply);
    }
}

