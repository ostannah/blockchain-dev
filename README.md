# Hardhat Exploration

Project exploring the use of Hardhat as a development environment for minting ERC20 tokens. 

Objectives:
- Understand how to setup and configure a Hardhat project.
- Compare the use of Ether.js and Waffle versus Web3.js and Truffle.
- Understand how to migrate projects from Truffle to Hardhat.
