require("@nomiclabs/hardhat-truffle5");

module.exports = {
  solidity: {
    compilers: [
      {
        version: "0.7.3"
      },
      {
        version: "0.8.0",
      },
      {
        version: "0.5.0",
      }
    ],
  },
};
