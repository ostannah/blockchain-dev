const OwnershipToken = artifacts.require("OwnershipToken");

// Vanilla Mocha test. Increased compatibility with tools that integrate Mocha.
describe("OwnershipToken contract", function() {
  let accounts;
  let creator;

  const TOTAL_SUPPLY = 10;

  before(async function() {
    accounts = await web3.eth.getAccounts();
    creator = accounts[0];
    investor = accounts[1];
  });

  describe("Deployment", function() {

    it("Should deploy with initial TOTAL_SUPPLY", async function() {

      const ownershipToken = await OwnershipToken.new(TOTAL_SUPPLY, { from: creator });
      
      assert.equal(await ownershipToken.balanceOf(creator), TOTAL_SUPPLY);
    });

    it("Should transfer TRANSFER_AMOUNT", async function() {

      const TRANSFER_AMOUNT = 5;

      const ownershipToken = await OwnershipToken.new(TOTAL_SUPPLY, { from: creator });

      assert.equal(await ownershipToken.balanceOf(creator), TOTAL_SUPPLY);

      await ownershipToken.mint(investor, TRANSFER_AMOUNT, { from: creator } )
      
      assert.equal(await ownershipToken.balanceOf(investor), TRANSFER_AMOUNT);
    });

  });
});

